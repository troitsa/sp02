package ru.vlasova.iteco.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.io.IOException;

@Component
public class ExitCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() throws IOException {
        terminalService.getReader().close();
        System.exit(1);
    }

}