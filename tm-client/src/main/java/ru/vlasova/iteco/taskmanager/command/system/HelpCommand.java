package ru.vlasova.iteco.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

@Component
public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "help";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for(@NotNull final AbstractCommand command : stateService.getCommands()) {
            terminalService.print(command.getName() + ": " + command.getDescription());
        }
    }

}