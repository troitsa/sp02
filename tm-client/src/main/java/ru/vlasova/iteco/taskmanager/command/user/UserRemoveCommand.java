package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove user";
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        return roles;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final List<UserDTO> userList = userEndpoint.findAllUsers(token);
        if(userList == null) {
            terminalService.print("There are no users");
            return;
        }
        terminalService.print("Choose user:");
        for(@NotNull UserDTO u : userList) {
            terminalService.print(u.getLogin());
        }
        @Nullable final String userLogin = terminalService.readString();
        userEndpoint.removeUser(token, userEndpoint.checkUser(token, userLogin));
        terminalService.print("User removed");
    }

}
