package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Creating task. Set name: ");
        @Nullable final String name = terminalService.readString();
        terminalService.print("Input description: ");
        @Nullable final String description = terminalService.readString();
        terminalService.print("Set start date: ");
        @Nullable final String dateStart = terminalService.readString();
        terminalService.print("Set end date: ");
        @Nullable final String dateFinish = terminalService.readString();
        @Nullable final TaskDTO task = taskEndpoint.insertTask(token, userId, name, description, dateStart, dateFinish);
        if(task == null) return;
        taskEndpoint.persistTask(token, task);
        terminalService.print("Task created.");
    }

}