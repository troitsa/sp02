package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        taskEndpoint.removeAllTasksByUserId(token, userId);
        terminalService.print("All tasks deleted.");
    }

}