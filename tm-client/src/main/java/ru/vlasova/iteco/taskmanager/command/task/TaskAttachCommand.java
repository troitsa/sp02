package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskAttachCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_attach";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Attach task to project";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        terminalService.print("Choose the task and type the number");
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllTasksByUserId(token, userId);
        if (taskList == null) return;
        if (taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final TaskDTO task = taskEndpoint.getTaskByIndex(token, userId, index);
            if(task == null) return;
            terminalService.print("Attach task: " + task.getName() + ". Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = projectEndpoint
                    .getProjectByIndex(token, userId, projectIndex).getId();
            task.setProjectId(projectId);
            terminalService.print("Task attached.");
        }
    }

}