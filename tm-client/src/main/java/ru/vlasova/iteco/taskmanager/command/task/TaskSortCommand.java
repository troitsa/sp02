package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public class TaskSortCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_sort";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sorting tasks";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllTasksByUserId(token, userId);
        if(taskList == null || taskList.isEmpty()) {
            terminalService.print("There are no tasks. To create: task_create");
            return;
        }
        terminalService.print("Choose sorting option:\n" +
                "1 sorting by creating date\n"+
                "2 sorting by starting date\n"+
                "3 sorting by finish date\n"+
                "4 sorting by ready condition\n"+
                "or leave the field empty");
        @Nullable final String sortMode = terminalService.readString();
        Stream<TaskDTO> stream = taskEndpoint.sortTask(userId, sortMode).stream();
        stream.forEach(e -> terminalService.print(e.getName()));
    }

}
