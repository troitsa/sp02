package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskListCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllTasksByUserId(token, userId);
        if(taskList == null || taskList.size() == 0) {
            terminalService.print("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}