package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

@Component
public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_registration";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "To register new user";
    }

    @Override
    public void execute() throws Exception {
         terminalService.print("Creating user. Set login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set password: ");
        @Nullable final String password = terminalService.readString();
        if (login == null || password == null) return;
        @Nullable final UserDTO user = userEndpoint.insertUser(login, password);
        if (user == null) {
            terminalService.print("User is not registered. Try again");
        } else {
            userEndpoint.persistUser(user);
            terminalService.print("User is registered.");
        }
    }

}