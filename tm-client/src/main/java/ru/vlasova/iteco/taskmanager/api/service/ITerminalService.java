package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;

public interface ITerminalService {

    @Nullable
    String readString();

    void print(@Nullable String string);

    @NotNull
    BufferedReader getReader();
}
