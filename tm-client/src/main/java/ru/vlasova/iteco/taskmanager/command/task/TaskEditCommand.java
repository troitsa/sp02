package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.TaskDTO;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

@Component
public final class TaskEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public List<Role> getRole() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

    @Override
    @NotNull
    public String getName() {
        return "task_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected task";
    }

    @Override
    public void execute() throws Exception {
        validSession();
        System.out.println("Choose the task and type the number");
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAllTasksByUserId(token, userId);
        if (taskList != null && taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final TaskDTO task = taskEndpoint.getTaskByIndex(token, userId, index);
            if (task == null) return;
            terminalService.print("Editing task: " + task.getName() + ". Set new name: ");
            @Nullable final String name = terminalService.readString();
            if (name == null) return;
            task.setName(name);
            terminalService.print("Set new description: ");
            @Nullable final String description = terminalService.readString();
            if (description == null) return;
            task.setDescription(description);
            terminalService.print("Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = projectEndpoint
                    .getProjectByIndex(token, userId, projectIndex).getId();
            if (projectId == null) return;
            task.setProjectId(projectId);
            taskEndpoint.mergeTask(token, task);
            terminalService.print("Task edit.");
        }
    }

}