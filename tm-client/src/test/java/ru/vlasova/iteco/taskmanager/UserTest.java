package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.vlasova.iteco.taskmanager.api.endpoint.Role;
import ru.vlasova.iteco.taskmanager.api.endpoint.UserDTO;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class UserTest extends AbstractTest {

    @Test
    public void doLogin() throws Exception {
        @NotNull final UserDTO userT = createUser(userId);
        userEndpoint.persistUser(userT);
        @NotNull final UserDTO userLogin = userEndpoint.doLogin("USERTEST","test");
        assertEquals(userT.getLogin(), userLogin.getLogin());
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void checkUser() throws Exception {
        @NotNull final UserDTO userT = createUser(userId);
        userEndpoint.persistUser(userT);
        @NotNull final String userIdCheck = userEndpoint.checkUser(tokenUser,"USERTEST");
        assertEquals(userT.getId(), userIdCheck);
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void editUser() throws Exception {
        @NotNull final UserDTO user1 = userEndpoint.insertUser("One","test1");
        userEndpoint.persistUser(user1);
        @NotNull final UserDTO user2 = userEndpoint.findUser(user1.getId());
        user2.setRole(Role.ADMIN);
        userEndpoint.mergeUser(tokenUser, user2);
        assertEquals(Role.ADMIN, user2.getRole());
        userEndpoint.removeUser(tokenAdmin, user1.getId());
    }

    @Test
    public void findAllUsers() throws Exception {
        @NotNull final UserDTO user1 = userEndpoint.insertUser("One","test1");
        @NotNull final UserDTO user2 = userEndpoint.insertUser("Two","test2");
        @NotNull final List<UserDTO> userBefore = userEndpoint.findAllUsers(tokenAdmin);
        userEndpoint.persistUser(user1);
        userEndpoint.persistUser(user2);
        @NotNull final List<UserDTO> userAfter = userEndpoint.findAllUsers(tokenAdmin);
        Assert.assertTrue(userAfter.size() - userBefore.size() == 2);
        userEndpoint.removeUser(tokenAdmin, user1.getId());
        userEndpoint.removeUser(tokenAdmin, user2.getId());
    }

    @Test
    public void findUserBySession() throws Exception {
        @NotNull final UserDTO userT = createUser(userId);
        userEndpoint.persistUser(userT);
        @NotNull final String token = sessionEndpoint.getToken("USERTEST","test");
        assertEquals(userT.getLogin(),userEndpoint.findUserBySession(token, userT.getId()).getLogin());
        sessionEndpoint.removeSession(token);
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void findUser() throws Exception {
        @NotNull final UserDTO userT = createUser(userId);
        userEndpoint.persistUser(userT);
        assertEquals(userT.getLogin(),userEndpoint.findUser(userT.getId()).getLogin());
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void persistUser() throws Exception {
        @NotNull final UserDTO userT = createUser(userId);
        userEndpoint.persistUser(userT);
        Assert.assertNotNull(userEndpoint.findUser(userId));
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void mergeUser() throws Exception {
        @NotNull final UserDTO userT = userEndpoint.insertUser("One","test");
        userEndpoint.persistUser(userT);
        @NotNull UserDTO testUser = userEndpoint.findUser(userT.getId());
        testUser.setRole(Role.ADMIN);
        userEndpoint.mergeUser(tokenUser, testUser);
        assertEquals(Role.ADMIN, testUser.getRole());
        userEndpoint.removeUser(tokenAdmin, userT.getId());
    }

    @Test
    public void removeUser() throws Exception {
        @NotNull final UserDTO user1 = userEndpoint.insertUser("One","test1");
        userEndpoint.persistUser(user1);
        @NotNull UserDTO testUser = userEndpoint.findUser(user1.getId());
        assertEquals("One", userEndpoint.findUser(user1.getId()).getLogin());
        userEndpoint.removeUser(tokenAdmin, user1.getId());
        Assert.assertNull(userEndpoint.findUser(user1.getId()));
    }

    private UserDTO createUser(String userId) throws Exception {
        @NotNull final UserDTO userCreate = userEndpoint.insertUser("USERTEST","test");
        return userCreate;
    }
}
