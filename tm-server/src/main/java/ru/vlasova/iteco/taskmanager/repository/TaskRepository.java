package ru.vlasova.iteco.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, String> {

    List<Task> findAllByUserIdAndProjectId(String userId, String projectId);

    void deleteByUserIdAndId(String userId, String id);

    List<Task> findAllByUserId(String userId);

    List<Task> findAllByUserIdAndNameContainingOrDescriptionContaining(String userId,
                                                                       String searchString1,
                                                                       String searchString2);

    Task findByUserIdAndId(String userId, String id);

    void deleteAllByUserId(String userId);

    List<Task> findAllByUserIdOrderByDateCreateAsc(String userId);

    List<Task> findAllByUserIdOrderByDateStartAsc(String userId);

    List<Task> findAllByUserIdOrderByDateFinishAsc(String userId);

    List<Task> findAllByUserIdOrderByStatusAsc(String userId);

    List<Task> findAllByUserIdOrderByNameAsc(String userId);

}
