package ru.vlasova.iteco.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.entity.Session;

@Repository
public interface SessionRepository extends CrudRepository<Session, String> {

    void deleteBySignature(String signature);
}
