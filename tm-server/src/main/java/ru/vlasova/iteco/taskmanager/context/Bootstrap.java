package ru.vlasova.iteco.taskmanager.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;

import javax.xml.ws.Endpoint;

@Getter
@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private ISessionEndpoint sessionEndpoint;

    public Bootstrap() {
    }

    public void start() {
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
    }

}
