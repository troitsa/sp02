package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.dto.TaskDTO;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    public TaskDTO insertTask(@Nullable final String token,
                           @Nullable final String userId,
                           @Nullable final String name,
                           @Nullable final String description,
                           @Nullable final String dateStart,
                           @Nullable final String dateFinish) throws Exception {
        validateSession(token);
        @Nullable final User user = userService.findOne(userId);
        return toTaskDTO(taskService.insert(user, name, description, dateStart, dateFinish));
    }

    @Override
    public @Nullable TaskDTO getTaskByIndex(@Nullable final String token,
                                         @Nullable final String userId,
                                         int index) throws Exception {
        validateSession(token);
        return toTaskDTO(taskService.getTaskByIndex(userId, index));
    }

    @Override
    public @Nullable List<TaskDTO> getTasksByProjectId(@Nullable final String token,
                                                    @Nullable final String userId,
                                                    @Nullable final String projectId) throws Exception {
        validateSession(token);
        return taskService
                .getTasksByProjectId(userId, projectId)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public void removeTaskByUserId(@Nullable final String token,
                                   @Nullable final String userId,
                                   @Nullable final String id) throws Exception {
        validateSession(token);
        taskService.remove(userId, id);
    }

    @Override
    public void removeTaskByIndex(@Nullable final String token,
                                  @Nullable final String userId,
                                  int index) throws Exception {
        validateSession(token);
        taskService.remove(userId,index);
    }

    @Override
    public @Nullable List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception {
        validateSession(token);
        return taskService.findAll()
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable List<TaskDTO> findAllTasksByUserId(@Nullable final String token,
                                                     @Nullable final String userId) throws Exception {
        validateSession(token);
        return taskService
                .findAll(userId).stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable TaskDTO findOneTask(@Nullable final String token,
                                      @Nullable final String id) throws Exception {
        validateSession(token);
        return toTaskDTO(taskService.findOne(id));
    }

    @Override
    public @Nullable TaskDTO findOneTaskByUserId(@Nullable final String token,
                                              @Nullable final String userId,
                                              @Nullable final String id) throws Exception {
        validateSession(token);
        return toTaskDTO(taskService.findOneByUserId(userId, id));
    }

    @Override
    @Nullable
    public void persistTask(@Nullable final String token,
                            @Nullable final TaskDTO task) throws Exception {
        validateSession(token);
        taskService.persist(toTask(task));
    }

    @Override
    public void mergeTask(@Nullable final String token,
                          @Nullable final TaskDTO task) throws Exception {
        validateSession(token);
        taskService.merge(toTask(task));
    }

    @Override
    public void removeTask(@Nullable final String token,
                           @Nullable final String id) throws Exception {
        validateSession(token);
        taskService.remove(id);
    }

    @Override
    public void removeAllTasks(@Nullable final String token) throws Exception {
        validateSession(token);
        taskService.removeAll();
    }

    @Override
    public void removeAllTasksByUserId(@Nullable final String token,
                                       @Nullable final String userId) throws Exception {
        validateSession(token);
        taskService.removeAll(userId);
    }

    @Override
    @NotNull
    public List<TaskDTO> searchTask(@Nullable final String token,
                                    @Nullable final String userId,
                                    @Nullable final String searchString) throws Exception {
        validateSession(token);
        return taskService
                .search(userId, searchString)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<TaskDTO> sortTask(@Nullable final String userId,
                                  @Nullable final String sortMode) {
        return taskService
                .sortTask(userId, sortMode)
                .stream()
                .map(this::toTaskDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    private Task toTask(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return null;
        @Nullable final Task task = new Task();
        task.setId(taskDTO.getId());
        User user = userService.findOne(taskDTO.getUserId());
        Project project = projectService.findOne(taskDTO.getProjectId());
        if (user == null) return null;
        task.setUser(user);
        if (project != null)
        task.setProject(project);
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateCreate(taskDTO.getDateCreate());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setStatus(taskDTO.getStatus());
        return task;
    }

    @Nullable
    private TaskDTO toTaskDTO(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        if(task.getProject() != null)
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateCreate(task.getDateCreate());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }
}