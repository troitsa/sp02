package ru.vlasova.iteco.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    User findByLogin(String login);

}