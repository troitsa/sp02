package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @Override
    @Nullable
    public List<Task> getTasksByProjectId(@Nullable final String userId,
                                          @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        @NotNull final List<Task> taskList = repository.findAllByUserIdAndProjectId(userId, projectId);
        return taskList;
    }

    @Override
    @Nullable
    public Task insert(@Nullable final User user, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || user == null) return null;
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(DateUtil.parseDateFromString(dateStart));
        task.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return task;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) {
        if (userId == null) return;
        @Nullable final Task task = getTaskByIndex(userId, index);
        if (task == null) return;
        repository.deleteByUserIdAndId(userId, task.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        if(userId == null || userId.isEmpty() || id == null || id.isEmpty()) return;
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Nullable
    public Task getTaskByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0 ) return null;
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        return taskList.get(index);
    }

    @Override
    @Nullable
    public List<Task> search(@Nullable final String userId, @Nullable final String searchString) {
        if (userId == null || searchString == null || searchString.isEmpty()) return null;
        @NotNull final List<Task> taskList = repository.
                findAllByUserIdAndNameContainingOrDescriptionContaining(userId, searchString, searchString);
        return taskList;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        @NotNull final List<Task> taskList = repository.findAllByUserId(userId);
        return taskList;
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        repository.save(task);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        @NotNull final List<Task> taskList = (List<Task>)repository.findAll();
        return taskList;
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = repository.findById(id).orElse(null);
        return task;
    }

    @Override
    @Nullable
    public Task findOneByUserId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final Task task = repository.findByUserIdAndId(userId, id);
        return task;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        repository.deleteAllByUserId(userId);
    }

    @Override
    public @Nullable List<Task> sortTask(@Nullable final String userId,
                                         @Nullable final String sortMode) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortMode != null || !sortMode.isEmpty()) {
            switch (sortMode) {
                case ("1"):
                    return repository.findAllByUserIdOrderByDateCreateAsc(userId);
                case ("2"):
                    return repository.findAllByUserIdOrderByDateStartAsc(userId);
                case ("3"):
                    return repository.findAllByUserIdOrderByDateFinishAsc(userId);
                case ("4"):
                    return repository.findAllByUserIdOrderByStatusAsc(userId);
            }
        }
        return repository.findAllByUserIdOrderByNameAsc(userId);
    }

}
