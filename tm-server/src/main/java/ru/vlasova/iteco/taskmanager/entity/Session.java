package ru.vlasova.iteco.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.persistence.*;
import org.hibernate.annotations.Cache;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "app_session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "entity.cache")
public class Session extends AbstractEntity {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne
    @JsonFilter("idFilter")
    private User user;

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Nullable
    private String signature;

    @Column(columnDefinition = "BIGINT", updatable = false)
    private long createDate = new Date().getTime();

}
