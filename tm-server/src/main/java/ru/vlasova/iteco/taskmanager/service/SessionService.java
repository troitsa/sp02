package ru.vlasova.iteco.taskmanager.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vlasova.iteco.taskmanager.api.service.IPropertyService;
import ru.vlasova.iteco.taskmanager.api.service.ISessionService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.dto.SessionDTO;
import ru.vlasova.iteco.taskmanager.entity.Session;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.AccessDeniedException;
import ru.vlasova.iteco.taskmanager.repository.SessionRepository;
import ru.vlasova.iteco.taskmanager.util.AESUtil;
import ru.vlasova.iteco.taskmanager.util.HashUtil;
import ru.vlasova.iteco.taskmanager.util.SignatureUtil;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public String getToken(@Nullable final String login, @Nullable String password) throws Exception {
        @Nullable final Session session = create(login,password);
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(toSessionDTO(session));
        return AESUtil.encrypt(json, key);
    }

    @Nullable
    @Override
    public Session create(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findOne(userService.checkUser(login));
        if (user == null) return null;
        password = HashUtil.MD5(password);
        if (password == null) return null;
        if (!password.equals(user.getPwd())) throw new AccessDeniedException("Password incorrect");
        Session userSession = new Session();
        userSession.setUser(user);
        userSession.setRole(user.getRole());
        userSession.setSignature(SignatureUtil.sign(toSessionDTO(userSession),
                propertyService.getSessionSalt(),
                propertyService.getSessionCycle()));
        persist(userSession);
        return userSession;
    }

    @Override
    public void validate(@Nullable final String token) throws Exception {
        Session currentSession = toSession(decryptToken(token));
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        if (!contains(currentSession.getId())) throw new AccessDeniedException("Invalid session");
        if (currentSession.getUser() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getSignature() == null) throw new AccessDeniedException("Invalid session");
        if (currentSession.getRole() == null) throw new AccessDeniedException("Invalid session");
        @NotNull final Session session = new Session();
        session.setSignature(currentSession.getSignature());
        session.setRole(currentSession.getRole());
        session.setId(currentSession.getId());
        session.setCreateDate(currentSession.getCreateDate());
        session.setUser(currentSession.getUser());
        @Nullable final String sessionSignature = SignatureUtil.sign
                (toSessionDTO(session), propertyService.getSessionSalt(), propertyService.getSessionCycle());
        @Nullable final String currentSessionSignature = SignatureUtil.sign
                (toSessionDTO(currentSession), propertyService.getSessionSalt(), propertyService.getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new AccessDeniedException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new AccessDeniedException("Invalid session");
        if (currentSession.getCreateDate() - new Date().getTime() > propertyService.getSessionLifetime())
            throw new AccessDeniedException("Invalid session");
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final List<Session> users = (List<Session>) repository.findAll();
        return users;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final Session session = repository.findById(id).orElse(null);
        return session;
    }

    @Override
    public void persist(@Nullable final Session userSession) {
        if (userSession == null) return;
        repository.save(userSession);
    }

    @Override
    public void merge(@Nullable final Session userSession) {
        if (userSession == null) return;
        repository.save(userSession);
    }

    @Override
    public void remove(@Nullable final String token) throws Exception {
        if (token == null) return;
        Session currentSession = toSession(decryptToken(token));
        if (currentSession.getSignature() == null) return;
        repository.deleteBySignature(currentSession.getSignature());
    }

    @Override
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    public boolean contains(@Nullable final String sessionId) {
        if (sessionId == null) return false;
        @NotNull final boolean contains = repository.existsById(sessionId);
        return contains;
    }

    @Override
    public void checkSession(@Nullable final String token, @NotNull final Role role) throws Exception {
        if (token == null) throw new AccessDeniedException("Invalid session");
        validate(token);
        @NotNull final Session session = toSession(decryptToken(token));
        if (session.getRole() != role) throw new AccessDeniedException("Access denied");
    }

    private SessionDTO decryptToken(@Nullable final String token) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final String json = AESUtil.decrypt(token, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionDTO sessionDTO = mapper.readValue(json, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    @NotNull
    public String getCurrentUserId(@Nullable final String token) throws Exception {
        @Nullable final Session currentSession = toSession(decryptToken(token));
        if (currentSession == null) throw new AccessDeniedException("Invalid session");
        return currentSession.getUser().getId();
    }

    @Nullable
    private SessionDTO toSessionDTO(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setRole(session.getRole());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setCreateDate(session.getCreateDate());
        return sessionDTO;
    }

    @Nullable
    private Session toSession(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final Session session = new Session();
        session.setId(sessionDTO.getId());
        User user = userService.findOne(sessionDTO.getUserId());
        if (user == null) return null;
        session.setUser(user);
        session.setRole(sessionDTO.getRole());
        session.setSignature(sessionDTO.getSignature());
        session.setCreateDate(sessionDTO.getCreateDate());
        return session;
    }
}