package ru.vlasova.iteco.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class SessionDTO extends AbstractDTO {

    @Getter
    @Setter
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String userId;

    @Nullable
    private Role role;

    @Nullable
    private String signature;

    @NotNull
    private long createDate = new Date().getTime();

}
