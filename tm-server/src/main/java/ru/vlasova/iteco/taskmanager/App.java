package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vlasova.iteco.taskmanager.config.ApplicationConfig;
import ru.vlasova.iteco.taskmanager.context.Bootstrap;

public final class App {

    public static void main(@Nullable final String[] args) {

        @NotNull
        final ApplicationContext context = new AnnotationConfigApplicationContext("ru.vlasova.iteco.taskmanager");

        @NotNull
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();

    }

}