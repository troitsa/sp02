package ru.vlasova.iteco.taskmanager.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.dto.UserDTO;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.vlasova.iteco.taskmanager.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    public @Nullable UserDTO insertUser(@Nullable final String login,
                                        @Nullable final String password) {
        return toUserDTO(userService.insert(login, password));
    }

    @Override
    public @Nullable UserDTO doLogin(@Nullable final String login,
                                  @Nullable final String password) throws Exception {
        return toUserDTO(userService.doLogin(login, password));
    }

    @Override
    public @Nullable String checkUser(@Nullable final String token,
                                      @Nullable final String login) throws Exception {
        validateSession(token);
        return userService.checkUser(login);
    }

    @Override
    public void editUser(@Nullable final String token,
                         @Nullable final UserDTO userDTO,
                         @Nullable final String login,
                         @Nullable final String password) throws Exception {
        validateSession(token);
        userService.edit(toUser(userDTO), login, password);
    }

    @Override
    public @Nullable List<UserDTO> findAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        return userService
                .findAll().stream()
                .map(this::toUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public @Nullable UserDTO findUserBySession(@Nullable final String token,
                                      @Nullable final String id) throws Exception {
        validateSession(token);
        return toUserDTO(userService.findOne(id));
    }

    @Override
    public @Nullable UserDTO findUser(@Nullable final String id) throws Exception {
        return toUserDTO(userService.findOne(id));
    }

    @Override
    public void persistUser(@Nullable final UserDTO userDTO) throws Exception {
        userService.persist(toUser(userDTO));
    }

    @Override
    public void mergeUser(@Nullable final String token,
                          @Nullable final UserDTO userDTO) throws Exception {
        validateSession(token);
        userService.merge(toUser(userDTO));
    }

    @Override
    public void removeUser(@Nullable final String token,
                           @Nullable final String id) {
        userService.remove(id);
    }

    @Override
    public void removeAllUsers(@Nullable final String token) throws Exception {
        validateSession(token);
        userService.removeAll();
    }

    @Override
    public Boolean checkRole(@Nullable String userId,
                             @NotNull List<Role> roles) {
        return userService.checkRole(userId, roles);
    }

    @Nullable
    private UserDTO toUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPwd(user.getPwd());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Nullable
    private User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @Nullable final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPwd(userDTO.getPwd());
        user.setRole(userDTO.getRole());
        return user;
    }

}
