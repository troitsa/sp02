package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User insert(@Nullable final String login,
                @Nullable final String password);

    @Nullable
    User doLogin(@Nullable final String login,
                 @Nullable final String password) throws Exception;

    @Nullable
    String checkUser(@Nullable final String login);

    void edit(@Nullable final User user,
              @Nullable final String login,
              @Nullable final String password);

    boolean checkRole(@Nullable final String userId,
                      @Nullable final List<Role> roles);

    @Nullable
    List<User> findAll();

    @Nullable
    User findOne(@Nullable final String id);

    void persist(@Nullable final User obj);

    void merge(@Nullable final User obj);

    void remove(@Nullable final String id);

    void removeAll();

}