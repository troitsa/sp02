package ru.vlasova.iteco.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskDTO insertTask(@Nullable final String token, @Nullable final String userId, @Nullable final String name,
                       @Nullable final String description, @Nullable final String dateStart,
                       @Nullable final String dateFinish) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO getTaskByIndex(@Nullable final String token,
                        @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> getTasksByProjectId(@Nullable final String token,
                                   @Nullable final String userId,
                                   @Nullable final String projectId) throws Exception;

    @WebMethod
    void removeTaskByUserId(@Nullable final String token,
                            @Nullable final String userId,
                            @Nullable final String id) throws Exception;

    @WebMethod
    void removeTaskByIndex(@Nullable final String token,
                           @Nullable final String userId, int index) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> searchTask(@Nullable final String token,
                          @Nullable final String userId,
                          @Nullable final String searchString) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> findAllTasksByUserId(@Nullable final String token,
                                    @Nullable final String userId) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findOneTask(@Nullable final String token,
                     @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findOneTaskByUserId(@Nullable final String token,
                             @Nullable final String userId,
                             @Nullable String id) throws Exception;

    @WebMethod
    void persistTask(@Nullable final String token,
                     @Nullable final TaskDTO task) throws Exception;

    @WebMethod
    void mergeTask(@Nullable final String token,
                   @Nullable final TaskDTO task) throws Exception;

    @WebMethod
    void removeTask(@Nullable final String token,
                    @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final String token) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(@Nullable final String token,
                                @Nullable final String userId) throws Exception;

    @Nullable
    @WebMethod
    List<TaskDTO> sortTask(@Nullable final String userId,
                           @Nullable final String sortMode);

}
