package ru.vlasova.iteco.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class UserDTO extends AbstractDTO {

    @Getter
    @Setter
    @NotNull
    private String id;

    @Nullable
    private String login = "";

    @Nullable
    private String pwd = "";

    @NotNull
    private Role role = Role.USER;

}
