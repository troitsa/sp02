package ru.vlasova.iteco.taskmanager.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String> {

    void deleteByUserIdAndId(String userId, String id);

    List<Project> findAllByUserId(String userId);

    List<Project> findAllByUserIdAndNameContainingOrDescriptionContaining(String userId,
                                                                          String searchString1,
                                                                          String searchString2);

    Project findByIdAndUserId(String id, String userId);

    void deleteAllByUserId(String userId);

    List<Project> findAllByUserIdOrderByDateCreateAsc(String userId);

    List<Project> findAllByUserIdOrderByDateStartAsc(String userId);

    List<Project> findAllByUserIdOrderByDateFinishAsc(String userId);

    List<Project> findAllByUserIdOrderByStatusAsc(String userId);

    List<Project> findAllByUserIdOrderByNameAsc(String userId);
}
