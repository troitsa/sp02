package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;

public interface IService<E extends AbstractEntity> {

    default boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;
        return true;
    }

}
